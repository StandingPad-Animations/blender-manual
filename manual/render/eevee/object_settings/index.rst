
###################
  Object Settings
###################

Settings for objects and object data.

.. toctree::
   :maxdepth: 2

   object_data.rst
