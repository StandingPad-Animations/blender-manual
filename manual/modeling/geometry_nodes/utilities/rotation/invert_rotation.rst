.. index:: Geometry Nodes; Invert Rotation
.. _bpy.types.FunctionNodeInvertRotation:

********************
Invert Rotation Node
********************

.. figure:: /images/node-types_FunctionNodeInvertRotation.webp
   :align: right
   :alt: Invert Rotation node.

The *Invert Rotation* node inverts a rotation.

Inputs
======

Rotation
    Standard rotation value.

Outputs
=======

Rotation
    The inverted rotation.
