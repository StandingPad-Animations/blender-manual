.. _dope-sheet-mask:

****
Mask
****

This mode shows all the :doc:`masks </movie_clip/masking/introduction>` in the blend-file
(that have at least one layer) and lets you adjust their keyframes.

.. figure:: /images/editors_dope-sheet_mask.png

   The Mask mode of the Dope Sheet Editor.
